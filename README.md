[![pipeline status](https://gitlab.com/AaronErhardt/crab-tag/badges/master/pipeline.svg)](https://gitlab.com/AaronErhardt/crab-tag/-/commits/master)

# About

Ferris the crab is the cute mascot for [Rust](https://rust-lang.org "The Rust programming language").
He and his friends are playing tag!
**[Do you want to join? Try it out!](https://aaronerhardt.gitlab.io/crab-tag "The crab tag game with Ferris!")**

## Further links:
- [The Rust programming language](https://rust-lang.org "The Rust programming language")
- [Search for a Rustacean](https://www.rustaceans.org "rustaceans.org")
- [Home of Ferris the Crustacean](https://www.rustacean.net "rustacean.net")

# Technical details

This game only uses HTML, CSS and SVG. That's all. **No JavaScript**.  
All the logic is done using HTML input tags and CSS selectors.

## Why no JavaScript?

Nowadays most websites are bloated with JavaScript code used for tracking, fingerprinting, managing ads and rendering UI elements.
Apparently, some websites rely more on JavaScript than on HTML and CSS:
In some places JavaScript even replaces most of HTML and CSS which often makes sites and apps slow and memory consuming.
In my opinion this should be rather the other way around: Modern HTML and CSS allow to create responsible and beautiful interfaces that need no or only a few lines of JavaScript code.
**Actually, this game shows how much you can do without JavaScript**.

Using only as much JavaScript as necessary has other benefits, too:

+ Faster loading speed
+ Less network usage
+ More responsive user experience

In order to improve your website even more you can **replace JavaScript with WASM**:
WASM allows you to compile many different programming languages into bytecode that can be executed by the browser. This is not only much more efficient and faster than JavaScript but also saves network bandwith and allows you to use such awesome languages as **[Rust](https://rust-lang.org "The Rust programming language")**.

## What does Rust speed mean?

Rust speed refers to the speed of the Rust programming language.

[The official homepage of Rust](https://rust-lang.org) puts it like this:
_"Rust is blazingly fast and memory-efficient: with no runtime or garbage collector, it can power performance-critical services, run on embedded devices and easily integrate with other languages"_.

## Special Thanks

Thanks to [rustacean.net](https://www.rustacean.net) for assets of Ferris the crab and the inspiration for this game :)

Should you notice a bug, spelling mistake or anything related or should you have ideas for improvements or you'd like to contribute let me know!
Simply contact me at [via Email](mailto:aaron.erhardt@t-online.de "Write Aaron Erhardt").
